<?php
	echo $this->Form->create('Product');
	echo $this->Form->input('category_id', array('empty' => '- choose one -'), array('type'=>'select'));
	echo $this->Form->input('name');
	echo $this->Form->input('price');
	echo $this->Form->input('id', array('type' => 'hidden'));
	echo $this->Form->end('Save Product');