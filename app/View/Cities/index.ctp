<!-- table id="cities-table" class="display"-->
<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($cities as $city): ?>
        <tr>
            <td><?php echo $city['City']['id']; ?></td>
            <td><?php echo $city['City']['name']; ?></td>
        </tr>
        <?php endforeach; ?>
        <?php unset($city); ?>
    </tbody>
</table>