<h1><?php echo h($category['Category']['name']); ?> <small>(<?php
                echo $this->Html->link(
                    'Edit',
                    array('action' => 'edit', $category['Category']['id']));?>) (<?php
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $category['Category']['id']),
                    array('confirm' => 'Are you sure?'));?>)<small></h1>

<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Price</th>
        <th>Action</th>
        <th>Created</th>
    </tr>

    <!-- Here is where we loop through our $products array, printing out product info -->

    <?php foreach ($category['Product'] as $product): ?>
    <tr>
        <td><?php echo $product['id']; ?></td>
        <td>
            <?php echo $this->Html->link($product['name'],
array('controller' => 'products', 'action' => 'view', $product['id'])); ?>
        </td>
        <td>$<?php echo $product['price']; ?></td>
        <td><?php echo $this->Html->link('Edit',
array('controller' => 'products', 'action' => 'edit', $product['id'])); ?> | <?php
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $product['id']),
                    array('confirm' => 'Are you sure?'));?></td>
        <td><?php echo $product['created']; ?></td>
    </tr>
    <?php endforeach; ?>
</table>

<p><small>Created: <?php echo $category['Category']['created']; ?></small></p>


