<?php

	class ProductsController extends AppController 
	{
		public $name 	= 'Products'; //pluralization of the Model
		public $helpers = array('Html', 'form'); //include additional helpers using the global $helpers
		//public $session; // optional
		//public $models; // optional

		public function index()
		{
			$this->set('title_for_layout', 'Products List Page');
			$this->set('var_hello_world', 'Hello World');

			//list of Products from database (no-filters)
			$this->set('products', $this->Product->find('all'));

		}

		public function view($id = null) {
			$this->set('title_for_layout', 'View Product');
	        if (!$id) {
	            throw new NotFoundException(__('Invalid product'));
	        }

	        $product = $this->Product->findById($id);
	        if (!$product) {
	            throw new NotFoundException(__('Invalid product'));
	        }
	        $this->set('product', $product);
	    }



	    public function add() {
			$this->set('title_for_layout', 'Add Product');
	    	//$this->request->is() takes a single argument, which can be the request METHOD (get, put, post, delete) or some request identifier (ajax).

			$this->set('categories', $this->Product->Category->find('list'));

			//Detecting the Object
			$this->request->addDetector(
			    'product',
			    array('env' => 'REQUEST_METHOD', 'value' => 'POST')
			);
			
			//debug($this->request->is('product'));
			
			//Validating if received the Product or not
	    	if ($this->request->is('product')) {
	            $this->Product->create();
	            if ($this->Product->save($this->request->data)) {
	                $this->Session->setFlash(__('Your product has been saved.'));
	                return $this->redirect(array('action' => 'index'));
	            }
	            $this->Session->setFlash(__('Unable to add your product.'));
	        }
	    }


	    public function edit($id = null) {
			$this->set('title_for_layout', 'Edit Product');
			
			$this->set('categories', $this->Product->Category->find('list'));
			$this->set(compact('categories'));

		    if (!$id) {
		        throw new NotFoundException(__('Invalid product'));
		    }

		    $product = $this->Product->findById($id);
		    if (!$product) {
		        throw new NotFoundException(__('Invalid product'));
		    }

		    if ($this->request->is(array('product', 'put'))) {
		        $this->Product->id = $id;
		        if ($this->Product->save($this->request->data)) {
		            $this->Session->setFlash(__('Your product has been updated.'));
		            return $this->redirect(array('action' => 'index'));
		        }
		        $this->Session->setFlash(__('Unable to update your product.'));
		    }

		    if (!$this->request->data) {
		        $this->request->data = $product;
		    }
		}

	    public function delete($id) {
			$this->set('title_for_layout', 'Delete Product');
			
		    if ($this->request->is('get')) {
		        throw new MethodNotAllowedException();
		    }

		    if ($this->Product->delete($id)) {
		        $this->Session->setFlash(
		            __('The product with id: %s has been deleted.', h($id))
		        );
		        return $this->redirect(array('action' => 'index'));
		    }
		}
	}