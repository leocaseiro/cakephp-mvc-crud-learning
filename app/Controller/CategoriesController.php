<?php

	class CategoriesController extends AppController 
	{
		public $name 	= 'Categories'; //pluralization of the Model
		public $helpers = array('Html', 'form'); //include additional helpers using the global $helpers
		//public $session; // optional
		//public $models; // optional

		public function index()
		{
			$this->set('title_for_layout', 'Categories List Page');
			$this->set('var_hello_world', 'Hello World');

			//list of Categories from database (no-filters)
			$this->set('categories', $this->Category->find('all', array('Category.name' => 'DESC')));

		}

		public function view($id = null) {
			$this->set('title_for_layout', 'View Category');
	        if (!$id) {
	            throw new NotFoundException(__('Invalid category'));
	        }

	        $category = $this->Category->findById($id);
	        if (!$category) {
	            throw new NotFoundException(__('Invalid category'));
	        }
	        $this->set('category', $category);
	    }



	    public function add() {
			$this->set('title_for_layout', 'Add Category');
	    	//$this->request->is() takes a single argument, which can be the request METHOD (get, put, post, delete) or some request identifier (ajax).

			//Detecting the Object
			$this->request->addDetector(
			    'category',
			    array('env' => 'REQUEST_METHOD', 'value' => 'POST')
			);
			
			//debug($this->request->is('category'));
			
			//Validating if received the Category or not
	    	if ($this->request->is('category')) {
	            $this->Category->create();
	            if ($this->Category->save($this->request->data)) {
	                $this->Session->setFlash(__('Your category has been saved.'));
	                return $this->redirect(array('action' => 'index'));
	            }
	            $this->Session->setFlash(__('Unable to add your category.'));
	        }
	    }


	    public function edit($id = null) {
			$this->set('title_for_layout', 'Edit Category');

		    if (!$id) {
		        throw new NotFoundException(__('Invalid category'));
		    }

		    $category = $this->Category->findById($id);
		    if (!$category) {
		        throw new NotFoundException(__('Invalid category'));
		    }

		    if ($this->request->is(array('category', 'put'))) {
		        $this->Category->id = $id;
		        if ($this->Category->save($this->request->data)) {
		            $this->Session->setFlash(__('Your category has been updated.'));
		            return $this->redirect(array('action' => 'index'));
		        }
		        $this->Session->setFlash(__('Unable to update your category.'));
		    }

		    if (!$this->request->data) {
		        $this->request->data = $category;
		    }
		}

	    public function delete($id) {
			$this->set('title_for_layout', 'Delete Category');
			
		    if ($this->request->is('get')) {
		        throw new MethodNotAllowedException();
		    }

		    if ($this->Category->delete($id)) {
		        $this->Session->setFlash(
		            __('The category with id: %s has been deleted.', h($id))
		        );
		        return $this->redirect(array('action' => 'index'));
		    }
		}
	}