<?php

	class Product extends AppModel 
	{
		//public $name 		= 'product';
		//public $useTable = 'product'; force the table's name if there's no way being a plural like products in that case.

    	public $order = "Product.name asc";

		public $validate = array(
	        'category_id' => array(
	            'rule' => 'notEmpty'
	        ),'name' => array(
	            'rule' => 'notEmpty'
	        ),
	        'price' => array(
	            'rule' => 'notEmpty'
	        )
	    );

	    public $belongsTo = array(
	        'Category' => array(
	            'className' => 'Category',
	            'dependent' => true
	        ),
	    );
	}