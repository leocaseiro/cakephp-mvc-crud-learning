<?php //echo $var_hello_world; ?>
<table>
    <tr>
        <th>Id</th>
        <th>Category</th>
        <th>Name</th>
        <th>Price</th>
        <th>Action</th>
        <th>Created</th>
    </tr>

    <!-- Here is where we loop through our $products array, printing out product info -->

    <?php foreach ($products as $product): ?>
    <tr>
        <td><?php echo $product['Product']['id']; ?></td>
        <td><?php echo $product['Category']['name']; ?></td>
        <td>
            <?php echo $this->Html->link($product['Product']['name'],
array('controller' => 'products', 'action' => 'view', $product['Product']['id'])); ?>
        </td>
        <td>$<?php echo $product['Product']['price']; ?></td>
        <td><?php echo $this->Html->link('Edit',
array('controller' => 'products', 'action' => 'edit', $product['Product']['id'])); ?> | <?php
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $product['Product']['id']),
                    array('confirm' => 'Are you sure?'));?></td>
        <td><?php echo $product['Product']['created']; ?></td>
    </tr>
    <?php endforeach; ?>
    <?php unset($product); ?>
</table>
<?php 
    echo $this->Html->link(
        'Add Product',
        array('controller' => 'products', 'action' => 'add')
    ); 
?>