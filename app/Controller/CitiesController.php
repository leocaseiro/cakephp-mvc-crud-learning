<?php

	class CitiesController extends AppController 
	{
		public function index()
		{

			$this->set('title_for_layout', 'Cities List Page');

			$this->paginate = array(
		        'conditions' => array(
				        'City.name LIKE' => 'Ca%',
				    ),
		        'limit' => 10,
		        'order' => array('name' => 'desc')
		    );

		    $cities = $this->paginate('City'); //http://localhost/cakephp/cities/index/page:4

			$this->set('cities', $cities);
		}
	}