<h1><?php echo h($product['Category']['name']);?> - <?php echo h($product['Product']['name']); ?> <small>(<?php
                echo $this->Html->link(
                    'Edit',
                    array('action' => 'edit', $product['Product']['id']));?>) (<?php
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $product['Product']['id']),
                    array('confirm' => 'Are you sure?'));?>)<small></h1>

<p><small>Created: <?php echo $product['Product']['created']; ?></small></p>

<p>$<?php echo h($product['Product']['price']); ?></p>

