<?php //echo $var_hello_world; ?>
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Action</th>
        <th>Created</th>
    </tr>

    <!-- Here is where we loop through our $categories array, printing out category info -->

    <?php foreach ($categories as $category): ?>
    <tr>
        <td><?php echo $category['Category']['id']; ?></td>
        <td>
            <?php echo $this->Html->link($category['Category']['name'],
array('controller' => 'categories', 'action' => 'view', $category['Category']['id'])); ?>
        </td>
        <td><?php echo $this->Html->link('Edit',
array('controller' => 'categories', 'action' => 'edit', $category['Category']['id'])); ?> | <?php
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $category['Category']['id']),
                    array('confirm' => 'Are you sure?'));?></td>
        <td><?php echo $category['Category']['created']; ?></td>
    </tr>
    <?php endforeach; ?>
    <?php unset($category); ?>
</table>
<?php 
    echo $this->Html->link(
        'Add Category',
        array('controller' => 'categories', 'action' => 'add')
    ); 
?>