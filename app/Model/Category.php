<?php

	class Category extends AppModel 
	{
    	public $order = "Category.name asc";

		public $validate = array(
	        'name' => array(
	            'rule' => 'notEmpty'
	        )
	    );

		public $hasMany = array(
			'Product' => array(
				'className' => 'Product',
				'foreignKey' => 'category_id'
			)
		);
	}